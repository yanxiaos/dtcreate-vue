import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 时间格式化
import "./utils/date";
// 全局样式文件
import './styles/index.scss';

import axios from "axios";
import api from "./api";

Vue.config.productionTip = false;

Vue.prototype.$axios = axios

Vue.use(api)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
