/*
 * @Descripttion:
 * @version:
 * @Author: 郭云展
 * @Date: 2020-08-21 13:35:12
 * @LastEditors: yanxiao
 * @LastEditTime: 2021-12-16 17:31:30
 */

// 获取类型⬇⬇⬇
export function typeOf (o) {
	return Object.prototype.toString.call(o).match(/ ([^\]]+)/i)[1].toLowerCase();
}

// 深拷贝⬇⬇⬇
export function deeoClone (o) {
	let deeoClone = (o) => {
		let type = this.typeOf(o);
		if (['number', 'string', 'boolean', 'null', 'undefined', 'regexp'].indexOf(type) > -1) {
			return o;
		} else if (type === 'array') {
			let t = [];
			for (let i = 0, l = o.length; i < l; i++) {
				t.push(deeoClone(o[i]));
			}
			return t;
		} else {
			let t = {};
			for (let k in o) {
				t[k] = deeoClone(o[k]);
			}
			return t;
		}
	};
	return deeoClone(o);
}
// 测试数值是否在范围内 ⬇⬇⬇
export function testRange (item) {
	if (item.referenceValue) {
		let min = parseFloat(item.referenceValue.split('～')[0])
		let max = parseFloat(item.referenceValue.split('～')[1])
		return item.dataValue < min || item.dataValue > max ? '1' : '0'
	}
}
export function toymd (date) {
	if (date.length == 14) {
		return date.replace(/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/, "$1/$2/$3 $4:$5:$6")
	} else if (date.length == 8) {
		return date.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1/$2/$3")
	}
}
// 数组string化并保持被中括号包裹⬇⬇⬇
export function listStr (list) {
	let string = list.toString()
	return string.slice(0, 0) + '[' + string.slice(0, string.length) + ']'
}
// 判断Json是否为空 ⬇⬇⬇
export function isEmptyObject (obj) {
	for (var key in obj) {
		// break;
		return true
	}
	return false
}
// 随机生成指定数量的随机数 ⬇⬇⬇
export function getRandomCode (length) {
	if (length > 0) {
		var data = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
			"J",
			"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d",
			"e", "f",
			"g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		];
		var nums = "";
		for (var i = 0; i < length; i++) {
			var r = parseInt(Math.random() * 61);
			nums += data[r];
		}
		return nums;
	} else {
		return false;
	}
}
// 获取 starDay 到 endDay之间所有时间 ⬇⬇⬇
export function getDayAll (starDay, endDay) {

	var arr = [];
	var dates = [];

	// 设置两个日期UTC时间
	var db = new Date(starDay);
	var de = new Date(endDay);

	// 获取两个日期GTM时间
	var s = db.getTime() - 24 * 60 * 60 * 1000;
	var d = de.getTime() - 24 * 60 * 60 * 1000;

	// 获取到两个日期之间的每一天的毫秒数
	for (var i = s; i <= d;) {
		i = i + 24 * 60 * 60 * 1000;
		arr.push(parseInt(i))
	}

	// 获取每一天的时间  YY-MM-DD
	for (var j in arr) {
		var time = new Date(arr[j]).Format('yyyy-MM-dd');
		dates.push(time)
	}

	return dates
}

// 对全局参数进行监听⬇⬇⬇
export function watch ({
	target,
	key,
	config
}) {
	var newValue = config.defaultValue || ''
	Object.defineProperty(target, key, {
		enumerable: true,
		configurable: true,
		get () {
			return newValue;
		},
		set (val) {
			newValue = val
			config.set(val)

		},
	})
}
// 数组转json格式 ⬇⬇⬇
export function arrayToJson ({
	list,
	key,
	value
}) {
	let json = {}
	// console.warn(list, '刚进入的list')
	for (let i = 0; i < list.length; i++) {
		const item = list[i];
		json[item[key]] = item[value]
	}
	// console.warn(json, '转化处理完成后的json')
	return json

}
// 判断是否为空值 ⬇⬇⬇
export function isNullValue (value) {
	return [null, undefined, ''].includes(value)
}

// 监测是否全数字字符 ⬇⬇⬇
export function isNumberValue (str) {
	var patrn = /^[0-9]*$/im;

	if (!patrn.test(str)) { // 如果不是纯数字返回false
		return false;
	}
	return true;
}
// 监测身份证格式是否正确 ⬇⬇⬇
export function isIdCardValue (str) {
	var patrn = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/im;

	if (!patrn.test(str)) { // 如果正确返回false
		return false;
	}
	return true;
}
// 监测手机号格式是否正确 ⬇⬇⬇
export function isPhoneNoValue (str) {
	var patrn = /^1[3|4|5|7|8][0-9]{9}$/im;

	if (!patrn.test(str)) { // 如果正确返回false
		return false;
	}
	return true;
}

// 监测只能输入英文和数字 ⬇⬇⬇
export function isEnglishNumber (str) {
	var patrn = /^[0-9a-zA-Z_]*$/;

	if (!patrn.test(str)) { // 如果正确返回false
		return false;
	}
	return true;
}
// 是否是压缩文件 ↓↓↓
export function isZip (type) {
	return ['rar', '7z', 'zip', 'apz', 'ar', 'bz', 'car', 'dar', 'cpgz', 'f', 'ha', 'hbc', 'hbc2', 'hbe', 'hpk',
		'hyp', 'asar'
	].includes(type)
}

// 大文件相关配置获取 ↓↓↓
export function getBigFileConfig (key) {
	let config = JSON.parse(localStorage.getItem('bigFileConfig'))
	if (['chunckSize'].includes(key)) {
		let numList = config[key].split('*')
		let num = Number(numList[0])
		numList.forEach((item, idx) => {
			if (idx > 0) {
				num = num * Number(item)
			}
		})
		return num
	} else {
		return config[key]
	}
}
