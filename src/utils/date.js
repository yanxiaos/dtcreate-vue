/*
 * @Author: zhuangfeiyu
 * @Date: 2021-03-07 16:30:53
 * @LastEditTime: 2021-03-07 16:31:01
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \chongqing-parking\src\utils\date.js
 */
(function () {
  Date.prototype.Format = function (fmt) {
    let o = {
      'M+': this.getMonth() + 1,
      'd+': this.getDate(),
      'h+': this.getHours(),
      'm+': this.getMinutes(),
      's+': this.getSeconds(),
      'q+': Math.floor((this.getMonth() + 3) / 3),
      'S': this.getMilliseconds()
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + ''))
    }
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
    return fmt
  }
})()
