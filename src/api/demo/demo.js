/*
 * @Description:
 * @Author: yanxiao
 * @Github: https://github.com/yanxiaos
 * @Date: 2021-12-06 16:31:14
 * @LastEditors: yanxiao
 */
import request from "@/utils/request";

const prefix = ''

// 第一次登录
export function demo (param) {
  return request({
    url: prefix + "/demo",
    method: "get",
    param: param
  });
}