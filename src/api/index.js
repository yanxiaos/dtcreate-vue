/*
 * @Description: 注册接口
 * @Author: zhoucheng
 * @Github: https://github.com/hj327
 * @Date: 2021-03-25 09:40:05
 * @LastEditors: yanxiao
 * @LastEditTime: 2021-12-16 17:10:19
 */
// demo
import * as demo from "./demo/demo.js";

const install = function (Vue) {
  if (install.installed) {
    return (install.installed = true);
  }
  Object.defineProperties(Vue.prototype, {

    $demo: {
      get () {
        return demo;
      }
    },

  });
};

export default {
  install
};